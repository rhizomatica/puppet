# Class: rhizo_base::packages
#
# This module manages the packages not included in other modules
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class rhizo_base::packages {
  contain "rhizo_base::packages::$lsbdistcodename"
}

class rhizo_base::packages::common {

  include stdlib

  package { ['sngrep', 'tcpdump', 'tshark', 'sudo', 'curl', 'screen', 'tzdata', 'file', 'git',
             'bzip2', 'unzip', 'gdb', 'cron', 'gzip', 'iproute2', 'psmisc', 'dpkg', 'apt',
             'vim-common', 'vim-tiny', 'xxd', 'rsync', 'pkg-config']:
      ensure => latest,
      require => Class['rhizo_base::apt'],
  }

  package { [ 'libc-ares2', 'libc-bin', 'bind9-host', 'rsyslog', 'facter', 'libicu-dev' ]:
      ensure  => latest,
      require => Class['rhizo_base::apt'],
  }

  package { ['mosh', 'tmux', 'openvpn', 'lm-sensors', 'runit-systemd', 'sqlite3',
            'libffi-dev', 'apcupsd', 'expect', 'gawk', 'swig', 'g++', 'tinc',
            'rrdtool', 'dnsmasq', 'joe', 'htop', 'iperf3', 'iftop',
            'websocketd', 'fping', 'mtr-tiny', 'openssh-server', 'telnet', 'netcat-traditional',
            'apache2', 'libapache2-mod-php7.3', 'php', 'php7.3-pgsql', 'php-curl', 'php-cli', 'php7.3-gd',
            'php7.3-intl', 'php-gettext']:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
    }

  package { [ 'python-smpplib' ]:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
  }

  package { [ 'collectd', 'libxml2-utils' ]:
      ensure          => 'installed',
      install_options => ['--no-install-recommends'],
    }

  alternatives { 'nc':
    mode => 'manual',
    path => '/bin/nc.traditional',
  }

}

class rhizo_base::packages::bullseye inherits rhizo_base::packages::common {

  package { ['python3-unidecode', 'python3-dateutil', 'python3-yaml', 'python3-formencode',
            'python3-xmltodict', 'python3-psycopg2', 'python3-aiosqlite', 'python3-pip' ]:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
    }
}

class rhizo_base::packages::buster inherits rhizo_base::packages::common {

  package { [ 'libapache2-mod-php7.0', 'libapache2-mod-php5', 'php5' ]:
      ensure  => absent,
      require => Class['rhizo_base::apt'],
  }

  package { ['python-unidecode', 'python-dateutil', 'python-yaml', 'python-formencode',
            'python-xmltodict', 'python-psycopg2', 'python-pysqlite2' ]:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
    }

  package { ['libcdk5nc6', 'msmtp-mta', 'ncat', 'libgps23' ]:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
    }

  package { [ 'libssl1.1', 'libsofia-sip-ua0', 'libsqlite3-0', ]:
      ensure  => latest,
      require => Class['rhizo_base::apt'],
  }

  # FIXME: An Apache restart is required after this change.
  file_line { 'apache_php':
    ensure             => present,
    path               => '/etc/php/7.3/apache2/php.ini',
    line               => 'short_open_tag = On',
    match              => '^short_open_tag',
    append_on_no_match => false,
  }

  file_line { 'apache_php_gc':
    ensure             => present,
    path               => '/etc/php/7.3/apache2/php.ini',
    line               => 'session.gc_maxlifetime = 14400',
    match              => '^session.gc_maxlifetime',
    append_on_no_match => false,
  }
}
