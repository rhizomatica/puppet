# Class: rhizo_base::sems
#
# This module manages the sems package and config
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class rhizo_base::sems {

  $prefix           = "$rhizo_base::postcode$rhizo_base::pbxcode"
  $vpn_ip_address   = $rhizo_base::vpn_ip_address
  $vsat_ip_address  = hiera('rhizo::vsat_ip_address', '127.0.0.1')
  $vsat_serv_ip_address  = hiera('rhizo::vsat_serv_ip_address', '192.168.12.10')
  $rtp_mux_port     = hiera('rhizo::rtp_mux_port', '5000')
  $stream_min       = hiera('rhizo::rtp_mux_stream_min', 1)
  $dids             = hiera('rhizo::dids', '')

  package { ['sems', ]:
      ensure  => installed,
      require => Class['rhizo_base::apt'],
    }

  systemd::unit_file { 'sems.service':
      source   => "puppet:///modules/rhizo_base/systemd/sems.service",
      require  => Package['sems'],
    }

  file { '/etc/sems/sems.conf':
      content => template('rhizo_base/sems.conf.erb'),
      require => Package['sems'],
    }

  file { '/etc/sems/etc/mux-in.sbcprofile.conf':
      content => template('rhizo_base/mux-in.sbcprofile.conf.erb'),
      require => Package['sems'],
    }

  file { '/etc/sems/etc/mux-out.sbcprofile.conf':
      content => template('rhizo_base/mux-out.sbcprofile.conf.erb'),
      require => Package['sems'],
    }

  file { '/etc/sems/etc/ifname_ext.conf':
      content => template('rhizo_base/ifname_ext.conf.erb'),
      require => Package['sems'],
    }

  file { '/etc/sems/etc/':
      ensure  => directory,
      source  => 'puppet:///modules/rhizo_base/sems-etc',
      owner   => 'root',
      recurse => remote,
      purge   => false,
    }

  file { '/etc/freeswitch/sip_profiles/vsat.xml':
      content => template('rhizo_base/vsat.xml.erb'),
      require => Package['freeswitch'],
    }

  file {'/etc/freeswitch/sip_profiles/vsat':
      ensure  => directory,
      require => File['/etc/freeswitch'],
    }


  file { '/etc/freeswitch/sip_profiles/vsat/sems.xml':
      content => template('rhizo_base/sems.xml.erb'),
      require => Package['freeswitch'],
    }

  }
