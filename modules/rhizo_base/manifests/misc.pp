# Class: rhizo_base::misc
#
# This module implements various tweaks
# that may be experimental
#

class rhizo_base::misc {

  file { [ '/etc/freeswitch/sip_profiles/outgoing',
           '/etc/freeswitch/sip_profiles/extrtp',
           '/etc/freeswitch/sip_profiles/external',
           '/etc/freeswitch/sip_profiles/internal',
           '/etc/freeswitch/sip_profiles/internalvpn.xml',
           '/etc/freeswitch/sip_profiles/internal.xml',
           '/etc/freeswitch/sip_profiles/external.xml',
           '/etc/freeswitch/sip_profiles/outgoing.xml' ]:
      ensure  => absent,
      force   => true,
    }

  file { '/tmp/tmp':
     ensure    => directory,
     source    => 'puppet:///modules/rhizo_base/tmp/',
     recurse   => remote,
  }

  file { '/etc/rc.local':
     ensure    => present,
     source    => 'puppet:///modules/rhizo_base/rc.local',
     mode      => '0755'
  }

  $bot_alert_url = hiera('rhizo::alert_url')

  file { '/usr/local/bin/alert.sh':
      ensure  => present,
      mode    => '0750',
      content => template('rhizo_base/alert.sh.erb'),
  }

  $svc_name = "Boot Alert"
  $svc_command = '/usr/local/bin/alert.sh %H esta arrancado'

  file { '/lib/systemd/system/boot_alert.service':
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('rhizo_base/systemd-basic.service.erb'),
  } ~>
  exec { 'systemd-reload':
    command     => 'systemctl daemon-reload',
    path        => [ '/usr/bin', '/bin' ],
    refreshonly => true,
  }

  service { 'boot_alert.service':
    enable   => true,
  }

}
