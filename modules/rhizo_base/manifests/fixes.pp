# Class: rhizo_base::fixes
#
# This module manages various system fixes
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#

class rhizo_base::fixes {

  include rhizo_base::misc

  contain "rhizo_base::fixes::$operatingsystem"

  file { '/etc/tmux.conf':
      ensure  => present,
      source  => 'puppet:///modules/rhizo_base/tmux.conf',
  }

}

class rhizo_base::fixes::ubuntu {
#FSCK at boot
  file { '/etc/default/rcS':
      ensure  => present,
      source  => 'puppet:///modules/rhizo_base/etc/default/rcS',
    }

#Grub fix
  file { '/etc/default/grub':
      ensure  => present,
      source  => 'puppet:///modules/rhizo_base/etc/default/grub',
      notify  => Exec['update-grub'],
    }

  exec { 'update-grub':
      command     => '/usr/sbin/update-grub',
      refreshonly => true,
    }
}

class rhizo_base::fixes::debian {
  file { '/root/.bashrc':
      ensure  => present,
      source  => 'puppet:///modules/rhizo_base/bashrc'
    }

  file { '/var/lib/puppet/state':
      ensure  => link,
      target  => '/var/cache/puppet/state/'
    }

  file { '/etc/systemd/system/tmp.mount':
      ensure => present,
      source => '/usr/share/systemd/tmp.mount'
    }

  service { 'tmp.mount':
      provider => 'systemd',
      enable   => true,
      require  => File['/etc/systemd/system/tmp.mount']
    }

  file { '/etc/systemd/coredump.conf':
     ensure => present,
     source => 'puppet:///modules/rhizo_base/systemd/coredump.conf'
    }

  file { '/var/log/journal':
     ensure => directory,
     owner  => 'root',
  }

}
