# Class: rhizo_base::osmocom
#
# This module manages the Osmocom stack
#
# Parameters: none
#
# Actions:
#

class rhizo_base::osmocom {
  contain "rhizo_base::osmocom::$lsbdistcodename"
}

class rhizo_base::osmocom::bullseye inherits rhizo_base::osmocom::buster {
}

class rhizo_base::osmocom::buster inherits rhizo_base::osmocom::common {

  package { [ "osmocom-$repo" ]:
      ensure   => 'installed'
   }

  package { [ 'libosmoabis13', 'libosmotrau2' ]:
      ensure   => '1.5.2'
   }

  package { [ 'libosmonetif11' ]:
      ensure   => '1.4.0'
   }

  package { [ 'libosmo-ranap7' ]:
      ensure   => '1.5.1'
   }

  package { [ 'libosmocore' ]:
      ensure   => '1.9.2'
   }

  package { [ 'osmo-bsc', 'osmo-bsc-meas-utils' ]:
      schedule => 'offpeak',
      ensure   => '1.11.0.2.6c6ca~rhizomatica.production',
      require  => Class['rhizo_base::apt'],
      notify   => Exec['notify-osmo-restart'],
   }
  package { [ 'osmo-msc' ]:
      schedule => 'offpeak',
      require  => Class['rhizo_base::apt'],
      ensure   => '1.9.0.92.a021a~rhizomatica.production',
   }
  package { [ 'osmo-mgw', 'libosmo-mgcp-client12' ]:
      schedule => 'offpeak',
      require  => Class['rhizo_base::apt'],
      ensure   => '1.12.2.8.78a93~rhizomatica.production',
   }
  package { [ 'osmo-sgsn' ]:
      ensure   => "$repo" ? { "latest" => '1.11.1.2.01c5~rhizomatica.production', "nightly" => "installed" },
      require  => Class['rhizo_base::apt'],
   }
  package { [ 'libgtp6' ]:
      ensure   => "$repo" ? { "latest" => '1.11.0', "nightly" => "installed" },
      require  => Class['rhizo_base::apt'],
   }
  package { [ 'osmo-sip-connector' ]:
      ensure   => '1.6.1rhizo1b',
      require  => Class['rhizo_base::apt'],
    }
  package { [ 'osmo-hlr', 'libosmo-mslookup1', 'osmo-mslookup-utils',
               'libosmo-gsup-client0' ]:
      schedule => 'offpeak',
      ensure   => '1.7.0.10.6784~rhizomatica.production',
      require  => Class['rhizo_base::apt'],
    }
  package { [ 'osmo-stp' ]:
      ensure   => '1.8.1',
      require  => Class['rhizo_base::apt'],
    }

}

class rhizo_base::osmocom::common {

  include systemd
  $network_name    = $rhizo_base::network_name
  $mcc             = $rhizo_base::mcc
  $mnc             = $rhizo_base::mnc
  $a5              = "1"
  $bts             = hiera('rhizo::bts')
  $lcls            = $rhizo_base::lcls

  $smsc_password   = $rhizo_base::smsc_password
  $smpp_password   = $rhizo_base::smpp_password

  $hlr_db          = hiera('rhizo::hlr_db', '/var/lib/osmocom/hlr.db')
  $sms_db          = $rhizo_base::sms_db
  $mncc_codec      = $rhizo_base::mncc_codec

  $mncc_ip_address = $rhizo_base::mncc_ip_address
  $vpn_ip_address  = hiera('rhizo::vpn_ip_address')
  $sgsn_ip_address = hiera('rhizo::sgsn_ip_address')
  $ggsn_ip_address = hiera('rhizo::ggsn_ip_address')
  $repo            = hiera('rhizo::osmo_repo', 'latest')
  $ipa_name        = hiera('rhizo::ipa_name',
                           sprintf("%s-%s-%s-%s", $mcc, $mnc, $bts[0]['lac'],
                                   regsubst($vpn_ip_address,
                                            '^(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)$',
                                            '\\3-\\4')))
  $mgw_num_ep     =  hiera('rhizo::mgw_num_ep', $bts.size * 24)
  $osmo_local_bin  = hiera('rhizo::osmo_local_bin', [])

  package { [ "osmocom-nitb", 'libosmo-sigtran7',
              'libosmo-mgcp-client9', 'libosmonetif8' ]:
     ensure   => 'absent'
    }

  package { [ 'python3-osmopy-libs', 'python3-osmopy-utils',
            'systemd-coredump', 'libosmocore-utils' ]:
      ensure   => 'installed',
    }

  package {  [ 'libsmpp1', 'libosmo-sigtran9' ]:
      ensure   => 'installed',
      require  => Class['rhizo_base::apt']
    }

  $ts0_phys_chan = { "unknown"  => "CCCH",
                     "1002"     => "CCCH+SDCCH4",
                     "1020"     => "CCCH+SDCCH4",
                     "2050M"    => "CCCH",
                     "2050S"    => "CCCH",
                     "2100"     => "CCCH",
                     "UmSite"   => "CCCH",
                     "UmSite1"  => "CCCH",
                     "lime"     => "CCCH"
                   }
  $ts1_phys_chan = { "unknown"  => "SDCCH8",
                     "1002"     => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "1020"     => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "2050M"    => "SDCCH8",
                     "2050S"    => "SDCCH8",
                     "2100"     => "SDCCH8",
                     "UmSite"   => "SDCCH8",
                     "UmSite1"  => "SDCCH8",
                     "lime"     => "TCH/F_TCH/H_SDCCH8_PDCH"
                    }
  $ts7_phys_chan = { "unknown"  => "PDCH",
                     "1002"     => "PDCH",
                     "1020"     => "PDCH",
                     "2050M"    => "PDCH",
                     "2050S"    => "PDCH",
                     "2100"     => "PDCH",
                     "UmSite"   => "TCH/F",
                     "UmSite1"  => "TCH/H",
                     "lime"     => "TCH/F_TCH/H_SDCCH8_PDCH"
                    }
  $phys_chan_dyn = { "unknown"  => "TCH/F",
                     "1002"     => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "1020"     => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "2050M"    => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "2050S"    => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "2100"     => "TCH/F_TCH/H_SDCCH8_PDCH",
                     "UmSite"   => "TCH/F",
                     "UmSite1"  => "TCH/H",
                     "lime"     => "TCH/F_TCH/H_SDCCH8_PDCH"
                    }

  if "AMR" in $mncc_codec {
    $phys_chan = { "unknown"  => "TCH/H",
                   "1002"     => "TCH/H",
                   "1020"     => "TCH/H",
                   "2050M"    => "TCH/H",
                   "2050S"    => "TCH/H",
                   "2100"     => "TCH/H",
                   "UmSite"   => "TCH/F",
                   "UmSite1"  => "TCH/H",
                   "lime"     => "TCH/H"
                  }
} else {
    $phys_chan = { "unknown"  => "TCH/F",
                   "1002"     => "TCH/F",
                   "1020"     => "TCH/F",
                   "2050M"    => "TCH/F",
                   "2050S"    => "TCH/F",
                   "2100"     => "TCH/F",
                   "UmSite"   => "TCH/F",
                   "UmSite1"  => "TCH/F",
                   "lime"     => "TCH/F"
                  }
  }

  unless hiera('rhizo::local_osmobsc_cfg') == "1" {
    if ($bts[0]['type'] == "unknown") {
      file { '/etc/osmocom/osmo-bsc.cfg':
         ensure => file,
         content => template(
               'rhizo_base/osmo-bsc-head.erb',
               'rhizo_base/osmo-bsc-tail.erb'),
       }
    } else {
      file { '/etc/osmocom/osmo-bsc.cfg':
         ensure => file,
         content => template(
               'rhizo_base/osmo-bsc-head.erb',
               'rhizo_base/osmo-bsc-bts.erb',
               'rhizo_base/osmo-bsc-tail.erb'),
         notify   => Exec['notify-osmo-restart'],
      }
    }
  }

  file { '/etc/osmocom/stack':
      source  => 'puppet:///modules/rhizo_base/stack',
      owner   => 'root',
      mode    => '0750',
    }

  file { '/etc/osmocom/osmo-stp.cfg':
      content => template('rhizo_base/osmo-stp.cfg.erb'),
      require => Package['osmo-stp'],
    }

  file { '/etc/osmocom/osmo-hlr.cfg':
      content => template('rhizo_base/osmo-hlr.cfg.erb'),
      require => Package['osmo-hlr'],
    }

  file { '/etc/osmocom/osmo-msc.cfg':
      content => template('rhizo_base/osmo-msc.cfg.erb'),
      require => Package['osmo-msc'],
    }

  file { '/etc/osmocom/osmo-mgw.cfg':
      content => template('rhizo_base/osmo-mgw.cfg.erb'),
      require => Package['osmo-mgw'],
    }

  file { '/etc/osmocom/osmo-mgw2.cfg':
      content => template('rhizo_base/osmo-mgw2.cfg.erb'),
      require => Package['osmo-mgw'],
    }

  file { '/etc/osmocom/osmo-sip-connector.cfg':
      content => template('rhizo_base/osmo-sip-connector.cfg.erb'),
      require => Package['osmo-sip-connector'],
    }

  file { '/etc/osmocom/osmo-sgsn.cfg':
      content => template('rhizo_base/osmo-sgsn.cfg.erb'),
      require => Package['osmo-sgsn'],
    }

  systemd::dropin_file { 'bsc_override':
      filename => 'override.conf',
      unit     => 'osmo-bsc.service',
      content  => template('rhizo_base/osmo-bsc.override.erb')
  }

  systemd::dropin_file { 'msc_override':
      filename => 'override.conf',
      unit     => 'osmo-msc.service',
      content  => template('rhizo_base/osmo-msc.override.erb')
  }

  systemd::dropin_file { 'hlr-override':
      unit     => 'osmo-hlr.service',
      filename => 'override.conf',
      source   => 'puppet:///modules/rhizo_base/systemd/osmo-hlr.override'
  }


  file { '/etc/tinc/check_mdns_route':
      ensure   => present,
      source   => 'puppet:///modules/rhizo_base/check_mdns_route',
      mode     => '0750'
    }

  file { '/lib/systemd/system/osmo-mgw-msc.service':
      ensure   => present,
      source   => 'puppet:///modules/rhizo_base/systemd/osmo-mgw-msc.service',
    }

  file { '/usr/local/bin/vty':
      source  => 'puppet:///modules/rhizo_base/vty',
      owner   => 'root',
      mode    => '0755',
    }

  file { '/usr/lib/python2.7/dist-packages/osmopy':
      ensure  => link,
      target  => '../../python3/dist-packages/osmopy'
    }

  service { 'osmo-hlr':
    provider   => 'systemd',
    enable     => true,
    ensure     => 'running',
    subscribe  => Package['osmo-hlr'],
  }

  service { [ 'osmo-stp', 'osmo-bsc',
              'osmo-msc', 'osmo-mgw',
              'osmo-sgsn', 'osmo-sip-connector' ]:
    provider => 'systemd',
    enable   => true,
    ensure   => 'running',
    require  => Package[ 'python3-osmopy-utils', 'python3-osmopy-libs' ],
  }

  service { [ 'osmocom-nitb', 'osmo-mgw-msc' ]:
    provider => 'systemd',
    enable   => false,
    ensure   => 'stopped'
  }

  service { 'osmo-nitb':
    provider => 'runit',
    ensure   => 'stopped'
  }

  exec { 'notify-osmo-restart':
      command     => '/bin/echo 1 > /tmp/OSMO-dirty',
      refreshonly => true,
    }

}
