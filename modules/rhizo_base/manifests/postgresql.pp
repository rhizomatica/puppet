# Class: rhizo_base::postgresql
#
# This module manages the PostgreSQL database
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#

class rhizo_base::postgresql {
  contain "rhizo_base::postgresql::$operatingsystem"
}

class rhizo_base::postgresql::common {

  $pgsql_db   = $rhizo_base::pgsql_db
  $pgsql_user = $rhizo_base::pgsql_user
  $pgsql_pwd  = $rhizo_base::pgsql_pwd
  $pgsql_host = $rhizo_base::pgsql_host

  postgresql::server::db { $pgsql_db:
      user     => $pgsql_user,
      password => postgresql_password($pgsql_user, $pgsql_pwd),
    }

  postgresql::server::role { $pgsql_user:
      password_hash  => postgresql_password($pgsql_user, $pgsql_pwd),
      update_password => true
    }

  if (hiera('rhizo::remote_postgres', 0) == 1) {

      postgresql::server::pg_hba_rule { 'Network remote access':
          type => 'host',
          database => $pgsql_db,
          user => $pgsql_user,
          address => '10.23.100.0/24',
          auth_method => 'md5',
      }
  }
}

class rhizo_base::postgresql::ubuntu inherits rhizo_base::postgresql::common {

  class { 'postgresql::globals':
      manage_package_repo => true,
      version             => '9.3',
    }->
  class { 'postgresql::server':
    }

}

class rhizo_base::postgresql::debian inherits rhizo_base::postgresql::common {

  if (hiera('rhizo::remote_postgres', 0) == 1) {
     $listen = '*'
  } else {
     $listen = 'localhost'
  }

  if ($lsbdistcodename == 'buster') {
     $manage_repo = true
  } else {
     $manage_repo = false
  }

  class { 'postgresql::globals':
      manage_package_repo => $manage_repo,
      version             => '9.6',
    }->
  class { 'postgresql::server':
      listen_addresses  => $listen,
    }

}
