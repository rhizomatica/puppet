# Class: rhizo_base::apt
#
# This module manages the apt repositories
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#

class rhizo_base::apt {
  contain "rhizo_base::apt::$lsbdistcodename"
}

class rhizo_base::apt::common {

  $osmo_repo  = hiera('rhizo::osmo_repo', 'latest')

  $fs_version = "1.8.6"

  class { '::apt':
     update => {
       frequency => 'weekly',
     },
  }

  file { '/etc/apt/apt.conf.d/90unsigned':
      ensure  => present,
      content => 'APT::Get::AllowUnauthenticated "true";',
    }

  file { '/etc/apt/preferences.d/pinning.pref':
      ensure  => present,
      content => template("rhizo_base/$lsbdistcodename-pinning.pref.erb"),
    }

  file { '/etc/apt/sources.list.d/freeswitch.list':
      ensure  => absent,
    }

  apt::source { 'rhizomatica':
      location          => 'http://dev.rhizomatica.org/ubuntu/',
      release           => 'precise',
      repos             => 'main',
      allow_unsigned => true,
      include  => {
        'src' => false,
        'deb' => true,
      },
      require           => File['/etc/apt/apt.conf.d/90unsigned'],
    }

  apt::source { 'rhizo':
      location          => 'http://repo.rhizomatica.org/ubuntu/',
      release           => 'precise',
      repos             => 'main',
      allow_unsigned => true,
      include  => {
        'src' => false,
        'deb' => true,
      },
      require           => File['/etc/apt/apt.conf.d/90unsigned'],
    }

  apt::source { 'irontec':
      location    => 'http://packages.irontec.com/debian',
      release     => "${lsbdistcodename}",
      repos       => 'main',
      key         => {
        'id'      => '4FF7139B43073A436D8C2C4F90D20F5ED8C20040',
        'source'  => 'http://packages.irontec.com/public.key'
       }
    }

  package { 'apt-transport-https':
      ensure  => installed,
    }

  apt::key { 'osmocom':
      id      => '6B2A9F3792D15EB70D4E6A8F86A730B653725973',
      source  => 'https://downloads.osmocom.org/packages/osmocom:/latest/Debian_10/Release.key',
      ensure  => refreshed
    }
}

class rhizo_base::apt::bullseye inherits rhizo_base::apt::buster {
}

class rhizo_base::apt::buster inherits rhizo_base::apt::common {

  apt::source { 'osmocom':
      location    => "http://downloads.osmocom.org/packages/osmocom:/${osmo_repo}/Debian_10/",
      release     => './',
      repos       => '',
      key         => {
        'id'      => '6B2A9F3792D15EB70D4E6A8F86A730B653725973',
        'source'  => 'https://downloads.osmocom.org/packages/osmocom:/latest/Debian_10/Release.key'
       },
      notify      => Exec['apt_update'],
    }

  apt::source { 'osmocom-obs':
      location    => "https://www.rhizomatica.org/repo/rhizomatica:/production/Debian_10/",
      release     => './',
      repos       => '',
      key         => {
        'id'      => '4A836527D0AF454C0A667CF98A3BAF8CD37B735F',
        'source'  => 'https://obs.osmocom.org/projects/rhizomatica/public_key'
       },
      notify      => Exec['apt_update'],
    }

  apt::source { 'rhizo-buster':
      location    => 'http://repo.rhizomatica.org/debian/',
      release     => 'buster',
      repos       => 'main',
      key         => {
        'id'      => '857FD282A0CD2282207556C67DF075856A6A0AD5',
        'source'  => 'http://repo.rhizomatica.org/debian/repo.key'
       }
    }

   file { [ '/etc/apt/sources.list.d/rhizo-jessie.list',
            '/etc/apt/sources.list.d/rhizo-stretch.list',
            '/etc/apt/sources.list.d/nodesource.list' ]:
      ensure     => absent,
    }
}
