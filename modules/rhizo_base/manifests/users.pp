# Class: rhizo_base::users
#
# This module manages the users on the BSCs.
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class rhizo_base::users {

  $password_hash = $rhizo_base::password_hash
  $pgsql_pwd     = $rhizo_base::pgsql_pwd

  user { 'rhizomatica':
      ensure     => present,
      gid        => 'rhizomatica',
      groups     => ['sudo', 'dip', 'plugdev', 'netdev', 'operator'],
      membership => inclusive,
      home       => '/home/rhizomatica',
      password   => Sensitive($password_hash),
      uid        => '1000',
      purge_ssh_keys => true
  }

  group { 'tic':
     name       => 'tic',
     ensure     => present
   }

  user { 'tic':
      ensure     => present,
      uid        => '2000',
      gid        => 'users',
      groups     => 'operator',
      membership => inclusive,
      home       => '/home/tic',
      shell      => '/bin/bash',
      managehome => true,
      purge_ssh_keys => true,
  }

  user { 'osmocom':
      ensure     => present,
      uid        => '2060',
      gid        => 'users',
      groups     => [ 'adm', 'operator', 'systemd-journal'],
      membership => inclusive,
      home       => '/home/osmocom',
      shell      => '/bin/bash',
      managehome => true,
      purge_ssh_keys => true,
  }

  file { '/home/rhizomatica/.ssh/config':
      content  => template('rhizo_base/ssh_user_config.erb'),
      owner    => 'rhizomatica',
      group    => 'rhizomatica',
      mode     => '0600'
  }

  file { '/home/tic/.ssh/config':
      content  => template('rhizo_base/ssh_user_config.erb'),
      owner    => 'tic',
      group    => 'tic',
      mode     => '0600'
  }

  file { '/home/osmocom/.ssh/config':
      content  => template('rhizo_base/ssh_user_config.erb'),
      owner    => 'osmocom',
      mode     => '0600'
  }

  file { '/home/tic/.bash_aliases':
      content  => template('rhizo_base/bash_aliases.erb'),
      owner    => 'tic',
      group    => 'tic',
      mode     => '0640'
  }

  file { '/root/.ssh':
      ensure => directory
  }

  file { '/root/.ssh/bts_key':
      ensure  => present,
      mode    => '0600',
      content => hiera('rhizo::bts_key'),
  }

  file { '/home/tic/.ssh/bts_key':
      ensure  => present,
      mode    => '0600',
      owner    => 'tic',
      group    => 'tic',
      content => hiera('rhizo::bts_key'),
  }

  file { '/home/osmocom/.ssh/bts_key':
      ensure  => present,
      mode    => '0600',
      owner   => 'osmocom',
      content => hiera('rhizo::bts_key'),
  }

  # SSH Deploy key and config for gitlab
  file { '/root/.ssh/bsc_dev':
      ensure  => present,
      mode    => '0600',
      content => hiera('rhizo::bsc_dev_deploy_key'),
  }

  file { '/root/.ssh/config':
      ensure => present,
      source => 'puppet:///modules/rhizo_base/ssh/config',
  }


}
