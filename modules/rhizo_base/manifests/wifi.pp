# wifi.pp
#
# A basic Wifi service with
# captive portal and authentication via SMS OTP.
#
#

class rhizo_base::wifi {

  $pub_if          = $rhizo_base::stats_if
  $wifi_if         = $rhizo_base::wifi_if
  $wifi_ip         = $rhizo_base::wifi_ip_address
  $wifi_net        = $rhizo_base::wifi_net
  $wifi_dhcp_range = $rhizo_base::wifi_dhcp_range
  $wifi_dns        = $rhizo_base::wifi_dns

  file { '/etc/dnsmasq.wifi.conf':
      content  => template('rhizo_base/dnsmasq.conf.erb'),
      notify  => Service['dnsmasq-wifi']
    }

  file { '/lib/systemd/system/dnsmasq-wifi.service':
      ensure   => present,
      source   => 'puppet:///modules/rhizo_base/systemd/dnsmasq-wifi.service',
    }

  service { 'dnsmasq-wifi':
      provider => 'systemd',
      enable   => true,
      ensure   => 'running'
    }

  file { '/etc/apache2/sites-available/001-portal.conf':
      content => template('rhizo_base/portal.conf.erb'),
      require => Package['apache2']
    }
  ->file { '/etc/apache2/sites-enabled/001-portal.conf':
      ensure  => link,
      target  => '../sites-available/001-portal.conf',
      require => Package['apache2'],
      notify  => Service['apache2']
    }

  file { '/etc/apache2/ports.conf':
      ensure  => present,
      source   => 'puppet:///modules/rhizo_base/ports.conf',
      require => Package['apache2'],
      notify  => Service['apache2']
    }

  file { '/etc/apache2/mods-enabled/rewrite.load':
      ensure  => link,
      target  => '../mods-available/rewrite.load',
      require => Package['apache2'],
      notify  => Service['apache2']
    }

  firewall { '000 accept DHCP (udp port 67)':
    proto   => 'udp',
    iniface => $wifi_if,
    dport   => [53, 67],
    action  => 'accept',
  }
  ->firewall { '001 accept WWW Portal':
    proto   => 'tcp',
    iniface => $wifi_if,
    dport   => 81,
    action  => 'accept',
  }
  ->firewall { '002 accept ICMP':
    proto       => 'icmp',
    iniface     => $wifi_if,
    destination => $wifi_ip,
    action      => 'accept',
  }
  ->firewall { '003 drop mark 10':
    iniface    => $wifi_if,
    match_mark => 0xa,
    action     => 'drop',
  }

  firewall { '005 accept related established':
    chain   => 'FORWARD',
    proto   => 'all',
    iniface => $pub_if,
    state   => ['RELATED', 'ESTABLISHED'],
    action  => 'accept',
  }
  ->firewall { '006 drop mark 10':
    chain      => 'FORWARD',
    iniface    => $wifi_if,
    match_mark => 0xa,
    action     => 'drop',
  }

  firewall { '020 redirect to portal':
    table      => 'nat',
    chain      => 'PREROUTING',
    proto      => 'tcp',
    dport      => 80,
    iniface    => $wifi_if,
    match_mark => 0xa,
    jump       => 'DNAT',
    todest     => "$wifi_ip:81"
  }

  firewall { '021 nat masq from Wifi':
    table    => 'nat',
    chain    => 'POSTROUTING',
    source   => $wifi_net,
    outiface => $pub_if,
    jump     => 'MASQUERADE'
  }

}
