#!/bin/bash

# This script will copy a file into /tmp on all the BTS
# No overwrite checking will take place.

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"
. /home/rhizomatica/bin/vars.sh

for bts in "${!BTS[@]}" ; do

  scp $SSH_OPTS $1 root@${BTS[$bts]}:/tmp

done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
