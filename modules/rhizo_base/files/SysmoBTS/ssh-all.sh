#!/bin/bash

# This script will exec cmd on all the BTS

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"
. /home/rhizomatica/bin/vars.sh

for bts in "${!BTS[@]}" ; do

  ssh -t $SSH_OPTS root@${BTS[$bts]} $*

done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
