#!/bin/bash

# This script should be updated from time to time
# with nightly tasks for the BTS

#exit

# Consider when making modifications that
# running the script more than once is expected to be safe.

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi

SSH_OPTS="-q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"
. /home/rhizomatica/bin/vars.sh

# Ensure BTS has access to public internet.
echo "Check Masquerading.."
/var/SysmoBTS/chk_masq

for bts in "${!BTS[@]}" ; do

  _model=${MODEL[$bts]}
  echo -e "---- Config: BTS $bts is a $_model ----\n"

  if [ "$_model" == "UmSite" ] || [ "$_model" == "UmSite1" ] || [ "$_model" == "" ] || [ "$_model" == "2100" ] || \
  [ "$_model" == "DUG20" ] || [ "$_model" == "rbs2000" ] || [ "$_model" == "Fake" ] ; then
    echo -e "Do nothing for this BTS Model\n"
    continue
  fi

  #scp $SSH_OPTS authorized_keys root@${BTS[$bts]}:/home/root/.ssh/
  echo -n "Set Date --> "
  ssh $SSH_OPTS root@${BTS[$bts]} "date -s '$(date)'"
  if [ "$?" != "0" ] ; then
    echo "No ssh access to BTS?"
    continue
  fi

  # Commands to run on all BTS:
  #echo "Checking if BTS can access OPKG REPOS."
  #ssh $SSH_OPTS root@${BTS[$bts]} "echo nameserver 1.1.1.1 > /etc/resolv.conf; echo nameserver 9.9.9.9 >> /etc/resolv.conf"
  #ssh $SSH_OPTS root@${BTS[$bts]} "wget https://downloads.sysmocom.de -O /dev/null"
  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg clean; opkg update"
  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-upgradable"
  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg upgrade sysmobts-util osmo-bts osmo-pcu gps-utils gpsdate gpsd-conf gpsd-udev gpsd gpsd-gpsctl"

  _modelNR=$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util model-nr)

  if [ "$?" == "127" ]; then
    echo -e "\n---- No sysmobts-util? ----\n"
    continue
  fi

  _trxNR=$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util trx-nr)

  if [ "$?" != "0" ] ; then
    echo "TRX Number?"
    continue
  fi

  #ssh $SSH_OPTS root@${BTS[$bts]} "mkdir -p /etc/profile.d"
  #scp $SSH_OPTS alias.sh root@${BTS[$bts]}:/etc/profile.d/alias.sh
  #scp $SSH_OPTS osmo-pcu_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-pcu.cfg
  #scp $SSH_OPTS pcu bts mgr txp /usr/local/bin/vty root@${BTS[$bts]}:/bin/
  #ssh $SSH_OPTS root@${BTS[$bts]} "chmod 750 /bin/pcu /bin/bts /bin/mgr /bin/txp /bin/vty"

  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-upgradable | cut -f 1 -d ' ' | grep gps | xargs -r opkg upgrade"
  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-installed | grep kernel-module-ipt | xargs -r opkg remove --force-removal-of-dependent-package --force-remove"
  #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-installed | grep kernel-module-nf | xargs -r opkg remove --force-removal-of-dependent-package --force-remove"

  if [ "$_modelNR" == "65535" ] || [ "$_modelNR" == "1020" ] && [ "$_trxNR" == "255" ] ; then
    echo "Looks like a SysmoBTS ($_modelNR)"
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop osmo-pcu"
    #scp $SSH_OPTS osmo-pcu.1.2.0.70-697f root@${BTS[$bts]}:/usr/bin/osmo-pcu
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl start osmo-pcu"
    #ssh $SSH_OPTS root@${BTS[$bts]} "echo '$SITE-$bts' > /etc/hostname"
    #scp $SSH_OPTS master/ntp.conf root@${BTS[$bts]}:/etc/ntp.conf
    #scp $SSH_OPTS osmo-pcu_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-pcu.cfg
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop gpsd.socket ; systemctl disable gspd.socket; systemctl restart gpsd"
    #scp $SSH_OPTS osmo-bts_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-bts-sysmo.cfg
    #scp $SSH_OPTS bts pcu /usr/local/bin/vty root@${BTS[$bts]}:/bin/
    #ssh $SSH_OPTS root@${BTS[$bts]} "chmod 750 /bin/txp"
    #scp $SSH_OPTS gpsdate.service root@${BTS[$bts]}:/lib/systemd/system
    #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-upgradable | cut -f 1 -d ' ' | grep libosmoabis | xargs -r opkg upgrade"
    #ssh $SSH_OPTS root@${BTS[$bts]} "opkg upgrade sysmobts-util"
  fi

  if [ "$_modelNR" == "2050" ] && [ "$_trxNR" == "0" ] ; then
    # Master Verified.
    echo "BTS is a 2050 Master"
    #ssh $SSH_OPTS root@${BTS[$bts]} "echo '$SITE-Master-$bts' > /etc/hostname"
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop gpsd.socket ; systemctl disable gspd.socket; systemctl restart gpsd"
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop osmo-pcu"
    #scp $SSH_OPTS osmo-pcu.1.2.0.70-697f root@${BTS[$bts]}:/usr/bin/osmo-pcu
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl start osmo-pcu"
    #scp $SSH_OPTS osmo-bts_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-bts-sysmo.cfg
    #scp $SSH_OPTS bts pcu /usr/local/bin/vty root@${BTS[$bts]}:/bin/
    #scp $SSH_OPTS rc.local root@${BTS[$bts]}:/etc/rc.local
    #ssh $SSH_OPTS root@${BTS[$bts]} "opkg list-upgradable | cut -f 1 -d ' ' | grep gps | xargs -r opkg upgrade"
  fi

  if [ "$_trxNR" == "1" ] ; then
    # Slave Verified.
    echo "BTS is a 2050 Slave"
    #scp $SSH_OPTS slave/gpsdate root@${BTS[$bts]}:/etc/default/gpsdate
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop osmo-pcu"
    #scp $SSH_OPTS osmo-pcu.1.2.0.70-697f root@${BTS[$bts]}:/usr/bin/osmo-pcu
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl start osmo-pcu"
    #ssh $SSH_OPTS root@${BTS[$bts]} "echo '$SITE-Slave-$bts' > /etc/hostname"
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl enable osmo-pcu; systemctl start osmo-pcu"
    #ssh $SSH_OPTS root@${BTS[$bts]} "systemctl stop gpsd.socket ; systemctl disable gspd.socket; systemctl stop gpsd ; systemctl disable gpsd"
    #scp $SSH_OPTS osmo-pcu_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-pcu.cfg
    #scp $SSH_OPTS osmo-bts_$bts.cfg root@${BTS[$bts]}:/etc/osmocom/osmo-bts-sysmo.cfg
    #scp $SSH_OPTS bts pcu /usr/local/bin/vty root@${BTS[$bts]}:/bin/
    #ssh $SSH_OPTS root@${BTS[$bts]} "chmod 750 /bin/txp"
    #ssh $SSH_OPTS root@${BTS[$bts]} "opkg upgrade sysmobts-util osmo-bts osmo-pcu"
  fi
  echo -e "----\n"
done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
