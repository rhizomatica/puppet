#!/bin/bash

# This script will upgrade osmo-* daemons on the BTS
# Depending on circumstances, this might not be safe.

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"
. /home/rhizomatica/bin/vars.sh

for bts in "${!BTS[@]}" ; do

  ssh $SSH_OPTS root@${BTS[$bts]} "systemctl restart gpsdate"

  grep OPKG_CREDS base-feeds-nightly.conf > /dev/null
  if [ "$?" == "0" ]; then
    sed -i s/OPKG_CREDS/$OPKG_CREDS/g base-feeds-nightly.conf
  fi

  if [ "$1" != "noupdate" ] ; then
    echo "Updating Feed..."
    scp $SSH_OPTS base-feeds-nightly.conf root@${BTS[$bts]}:/etc/opkg/base-feeds.conf
    ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg update"
  fi

  ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg install osmo-bts osmo-pcu sysmobts-util ntp ipaccess-utils osmo-bts-remote"
  ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg remove libosmoabis6 libosmogsm13 libosmovty3 libosmovty4"
  ssh $SSH_OPTS root@${BTS[$bts]} "systemctl enable osmo-pcu; systemctl start osmo-pcu"

done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
