#!/bin/bash

exec 2> /dev/null

SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
. /home/rhizomatica/bin/vars.sh


for bts in "${!BTS[@]}" ; do

  _model=$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util model-nr)
  echo "BTS $bts is model $_model"
  if [ "$_model" != "2050" ]; then continue; fi

  if [ "$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util trx-nr)" == "0" ] ; then
    # Master Verified.
    echo "BTS $bts is a 2050 Master"
    ssh $SSH_OPTS ${BTS[$bts]} "osmo-bts-sysmo -V | grep BTS\ version; osmo-pcu -V | grep PCU\ version"
    ssh $SSH_OPTS ${BTS[$bts]} "uname -a"
    if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'grep 201705 /etc/opkg/base-feeds.conf >/dev/null; echo $?')" == "0" ] ; then
      echo "Feed is 201705"
    fi
  fi

  if [ "$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util trx-nr)" == "1" ] ; then
    # Slave Verified.
    echo "BTS $bts is a 2050 Slave"
    ssh $SSH_OPTS ${BTS[$bts]} "osmo-bts-sysmo -V | grep BTS\ version; osmo-pcu -V | grep PCU\ version"
    ssh $SSH_OPTS ${BTS[$bts]} "uname -a"
    if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'grep 201705 /etc/opkg/base-feeds.conf >/dev/null; echo $?')" == "0" ] ; then
      echo "Feed is 201705"
    fi
  fi

done
