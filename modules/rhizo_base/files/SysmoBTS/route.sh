#!/bin/bash

# Script to add the default route to the networking
# configuration.

# !!!!! WARNING !!!!!
# Be VERY careful with this one, as
# a messed up /etc/network/interfaces will lock
# you out of the unit and you'll have to physically
# connect with a serial cable.
# FIXME. Find a better way.


if [ "$1" != "reckless" ] ; then
  echo "Do NOT run this script!"
  exit 1
fi

SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
. /home/rhizomatica/bin/vars.sh


CMD='grep -xF "    gateway 172.16.0.1" /etc/network/interfaces || grep gateway\ :alpha: /etc/network/interfaces || echo "    gateway 172.16.0.1" >> /etc/network/interfaces'

for bts in "${!BTS[@]}" ; do

  awk 'BEGIN {while (x++<80) printf "-"}' ; echo
  ssh $SSH_OPTS ${BTS[$bts]} "cat /etc/network/interfaces"
  if [ $? != 0 ] ; then echo "Conx. Failed" ; exit ; fi
  awk 'BEGIN {while (x++<80) printf "-"}' ; echo

  echo "Should we add a gateway line to the above? (y/n)"
  read _ans

  if [ "$_ans" == "y" ] ; then

    ssh $SSH_OPTS ${BTS[$bts]} "$CMD"

  else

    echo "Copy interfaces file? (y/n)"
    read _ans
    if [ "$_ans" == "y" ] ; then

      _rfile=/tmp/interfaces-$RANDOM
      cat interfaces | sed s/_IP_/${BTS[$bts]}/ > $_rfile
      scp $_rfile ${BTS[$bts]}:/etc/network/interfaces
      rm $_rfile

    fi

  fi
    awk 'BEGIN {while (x++<80) printf "-"}' ; echo
    ssh $SSH_OPTS ${BTS[$bts]} "cat /etc/network/interfaces"
    awk 'BEGIN {while (x++<80) printf "-"}' ; echo
    echo "If that does not look OK, fix it now manually, otherwise you might be locked out on reboot"

done
