#!/bin/bash

grep isrgrootx1.pem /etc/ca-certificates.conf && exit

wget -q --no-check-certificate https://letsencrypt.org/certs/isrgrootx1.pem -O /usr/share/ca-certificates/isrgrootx1.pem
sed -i '/^mozilla\/AffirmTrust_Commercial.crt/i isrgrootx1.pem' /etc/ca-certificates.conf
sed -i '/^mozilla\/DST_Root_CA_X3/s/^/!/' /etc/ca-certificates.conf
/usr/sbin/update-ca-certificates


