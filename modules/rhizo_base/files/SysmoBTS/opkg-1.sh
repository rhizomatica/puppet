#!/bin/bash

# This script will upgrade osmo-* daemons on the BTS
# Depending on circumstances, this might not be safe.

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"

. /etc/profile.d/rccn-functions.sh
. /home/rhizomatica/bin/vars.sh

rm -rf ./opkg
mkdir opkg
mkdir opkg/cache

for bts in "${!BTS[@]}" ; do

  ssh $SSH_OPTS root@${BTS[$bts]} "systemctl restart gpsdate"
  if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'grep 201705-testing /etc/opkg/base-feeds.conf >/dev/null; echo $?')" == "1" ] ; then
    grep OPKG_CREDS base-feeds-testing.conf > /dev/null
    if [ "$?" == "0" ]; then
      sed -i s/OPKG_CREDS/$OPKG_CREDS/g base-feeds-testing.conf
    fi
    if [ $bts == 0 ]; then
      if [ "$1" != "noupdate" ] ; then
        scp $SSH_OPTS base-feeds-testing.conf root@${BTS[$bts]}:/etc/opkg/base-feeds.conf
        ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg clean; opkg update"
      fi
      ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg install osmo-pcu"
      scp $SSH_OPTS -r root@${BTS[$bts]}:/var/lib/opkg/* opkg
      scp $SSH_OPTS -r root@${BTS[$bts]}:/var/cache/opkg/* opkg/cache
    fi
    if [ $bts != 0 ] ; then
      scp $SSH_OPTS -r opkg/* root@${BTS[$bts]}:/var/lib/opkg/
      scp $SSH_OPTS -r opkg/cache/* root@${BTS[$bts]}:/var/cache/opkg/
      ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg install osmo-pcu"
    fi
    ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg remove libosmoabis6 libosmogsm13 libosmovty3 libosmovty4"
  fi 

  fi
 
done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
