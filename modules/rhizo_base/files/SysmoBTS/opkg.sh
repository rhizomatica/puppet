#!/bin/bash

# This script will upgrade osmo-* daemons on the BTS
# Depending on circumstances, this might not be safe.

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/tmp/known-$RANDOM"
. /home/rhizomatica/bin/vars.sh

for bts in "${!BTS[@]}" ; do

  ssh $SSH_OPTS root@${BTS[$bts]} "systemctl restart gpsdate"
  _modelNR=$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util model-nr)
  _trxNR=$(ssh $SSH_OPTS ${BTS[$bts]} sysmobts-util trx-nr)

  if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'grep 201705 /etc/opkg/base-feeds.conf >/dev/null; echo $?')" == "0" ] ; then

    grep OPKG_CREDS base-feeds-nightly.conf > /dev/null
    if [ "$?" == "0" ]; then
      sed -i s/OPKG_CREDS/$OPKG_CREDS/g base-feeds-nightly.conf
    fi
    if [ "$1" != "noupdate" ] ; then
      scp $SSH_OPTS base-feeds-nightly.conf root@${BTS[$bts]}:/etc/opkg/base-feeds.conf
      ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg update"
    fi
    ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg install osmo-bts osmo-pcu sysmobts-util ntp ipaccess-utils osmo-bts-remote"
    ssh $SSH_OPTS root@${BTS[$bts]} "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin opkg remove libosmoabis6 libosmogsm13 libosmovty3 libosmovty4"
    ssh $SSH_OPTS root@${BTS[$bts]} "systemctl enable osmo-pcu; systemctl start osmo-pcu"

  fi 

  #if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'ls /usr/bin/osmo-bts-sysmo-nitb 2> /dev/null; echo $?')" == "2" ] ; then
    gunzip -c osmo-bts-sysmo-arm-cf7a.gz > osmo-bts-sysmo-nitb
    scp $SSH_OPTS osmo-bts-sysmo-nitb root@${BTS[$bts]}:/usr/bin/
    ssh $SSH_OPTS root@${BTS[$bts]} "chmod 755 /usr/bin/osmo-bts-sysmo-nitb"
    ssh $SSH_OPTS root@${BTS[$bts]} "cd /usr/bin; mv osmo-bts-sysmo osmo-bts-sysmo.opkg ; ln -s osmo-bts-sysmo-nitb osmo-bts-sysmo"
  #fi
  #if [ "$(ssh $SSH_OPTS ${BTS[$bts]} 'ls /usr/bin/osmo-pcu-nitb 2> /dev/null; echo $?')" == "2" ] ; then
    gunzip -c osmo-pcu-arm-54211.gz > osmo-pcu-nitb
    scp $SSH_OPTS osmo-pcu-nitb root@${BTS[$bts]}:/usr/bin/
    ssh $SSH_OPTS root@${BTS[$bts]} "chmod 755 /usr/bin/osmo-pcu-nitb"
    ssh $SSH_OPTS root@${BTS[$bts]} "cd /usr/bin; mv osmo-pcu osmo-pcu.opkg ; ln -s osmo-pcu-nitb osmo-pcu; systemctl restart osmo-bts"
  #fi

done

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
 