#!/bin/bash

# Puppet has made some change that requires a Freeswitch restart.
# Place flag file anyway as fallback. Service will delete it
if [ ! -f /tmp/FS-dirty ] ; then
  echo 1 > /tmp/FS-dirty
fi

if [ `/usr/bin/fs_cli -x 'fsctl shutdown_check'` == "true" ] ; then
  exit
fi

# Trigger an "elegant" shutdown -
# (Shutdown when there are no active calls, but don't disable service in the meantime)
# This won't return until FS actually shuts down.
exec /usr/bin/fs_cli -x 'fsctl shutdown elegant' &

