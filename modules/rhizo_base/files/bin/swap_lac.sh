#!/bin/bash

. /home/rhizomatica/bin/vars.sh

_ALT_LAC=""
if [ -f /etc/osmocom/alt_lac ]; then
	_ALT_LAC=$(cat /etc/osmocom/alt_lac)
fi

if [ "$_ALT_LAC" == "" ]; then
	_ALT_LAC=1
else
	_ALT_LAC=""
fi
echo $_ALT_LAC > /etc/osmocom/alt_lac
/bin/sync
for bts in "${!BTS[@]}" ; do
	echo "BTS $bts:"
	/usr/bin/osmo_ctrl.py -d 127.0.0.1 -p 4249 -s bts.$bts.location-area-code $_ALT_LAC${LAC[$bts]}
	/usr/bin/osmo_ctrl.py -d 0 -p 4249 -s bts.$bts.send-new-system-informations 1
done
