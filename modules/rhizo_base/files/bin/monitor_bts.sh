#!/bin/bash
RHIZO_SCRIPT="/home/rhizomatica/bin"

. $RHIZO_SCRIPT/vars.sh

if [ ! -f /tmp/no_check_amps ] ; then
  $RHIZO_SCRIPT/monitor_amp.sh
fi

for bts in "${BTS[@]}" ; do

    if [ "$bts" == "" ] ; then continue ; fi

    _f="/tmp/bts.error.$bts"

    fping -q -c5 -p90 $bts >/dev/null 2>&1
    if [ $? == 1 ] ; then
	# BTS has a problem

	if [ -f $_f ] ; then
		# And it is not the first time, increment the counter.
		read _c < $_f
		((_c++))
		echo $_c > $_f
		if [ $((_c % 24)) == 0 ] ; then
			/usr/local/bin/alert.sh "$HOSTNAME: BTS $bts todavia no responde a ping (desde hace $((_c*5/60)) horas)." > /dev/null
		fi
	else
		# And this is a new condition, so initialise counter and alert
		echo 1 > $_f
		/usr/local/bin/alert.sh noenc "%F0%9F%98%BF" "$HOSTNAME: BTS $bts no responde a ping." > /dev/null
	fi
    else
	# BTS is OK
	if [ -f $_f ] ; then
		# This is cancelling a previous error condition, Remove counter and alert.
		rm $_f
		/usr/local/bin/alert.sh noenc "%F0%9F%98%BB" "$HOSTNAME: BTS $bts vuelve a responder a ping!" > /dev/null
	fi
fi
done
