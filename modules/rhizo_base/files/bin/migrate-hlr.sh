#!/bin/bash

echo "This will (re)-start daemons and disrupt service. Continue (y/n)?"

read answer

if [ "$answer" != "y" ]; then
  exit
fi

if [ "$PWD" != "/var/lib/osmocom" ]; then
  OLDPWD=$PWD
  cd /var/lib/osmocom
fi

apt install osmo-hlr
sv stop osmo-nitb
echo "PRAGMA wal_checkpoint(FULL)" | sqlite3 ./hlr.sqlite3
 
if [ -f hlr.db ]; then 
  echo "OSMO HLR hlr.db Exists, Making backup..."
  systemctl stop osmo-hlr
  echo "PRAGMA wal_checkpoint(FULL)" | sqlite3 ./hlr.db
  if [ -s hlr.db-wal ]; then
    echo "WAL file not empty. Exiting."
    systemctl start osmo-hlr
    exit
  fi
  mv hlr.db hlr.db.$(date +%F-%T)
  rm hlr.db-wal hlr.db-shm 2> /dev/null
fi

echo "Clean invalid IMSIs from NITB db"
echo 'DELETE FROM subscriber WHERE authorized=0 AND length(imsi)!=15;' | sqlite3 ./hlr.sqlite3

echo "Importing NITB db form hlr.sqlite3 to hlr.db..."
osmo-hlr-db-tool import-nitb-db hlr.sqlite3 -l hlr.db
systemctl restart osmo-hlr
echo "done"

/var/rhizomatica/rccn/OsmoSmsQ.py -p ./hlr.sqlite3 -q

if [ -f sms.db ]; then 
  systemctl stop osmo-msc
  echo "OSMO (S)MSC sms.db Exists, Making backup..."
  echo "PRAGMA wal_checkpoint(FULL)" | sqlite3 ./sms.db
  if [ -s sms.db-wal ]; then
    echo "WAL file not empty. Exiting."
    systemctl start osmo-msc
    exit
  fi
  mv sms.db sms.db.$(date +%F-%T)
  rm sms.db-wal sms.db-shm 2>/dev/null
fi

echo ".clone sms.db" | sqlite3 hlr.sqlite3
echo "Purging old Subscriber and Equipment records from SMS db..."
echo "DELETE FROM Equipment" | sqlite3 sms.db
echo "DELETE FROM EquipmentWatch" | sqlite3 sms.db
echo "DELETE FROM Subscriber" | sqlite3 sms.db
echo "DELETE FROM Counters" | sqlite3 sms.db
echo "VACUUM" | sqlite3 sms.db
echo "PRAGMA journal_mode=WAL;" | sqlite3 sms.db

/var/rhizomatica/rccn/OsmoSmsQ.py -p ./sms.db -q
mv sms.db sms_old.db
/var/rhizomatica/rccn/OsmoSmsQ.py -p ./sms_old.db -q -W sms.db
/var/rhizomatica/rccn/OsmoSmsQ.py -p ./sms.db -q

systemctl restart osmo-msc

cd $OLDPWD
