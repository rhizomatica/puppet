#!/bin/bash

# Hacks...

if [ "$PWD" != "/var/SysmoBTS" ]; then
  OLDPWD=$PWD
  cd /var/SysmoBTS
fi

# Nothing to do.

if [ "$OLDPWD" != "" ]; then
  cd $OLDPWD
fi
